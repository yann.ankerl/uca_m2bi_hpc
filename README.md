To run the pipeline use the command :

snakemake --cores 16 --jobs 24 --use-conda --snakefile workflows/Snakefile.rrbs.smk --cluster "sbatch --mem {resources.mem_mb} --time={resources.time} --output=logs/slurm-%j.out"

inside the uca_m2bi_hpc repository

# M2BI_snakemake

This is part of Master 2 Bioinformatique de l'Université Clermont Auvergne - Unité d'Enseignement "Calculs parallèles et initiation à la programmation GPU"


## Objectives

Objective is to introduce a Workflow management Language to reproduce analysis on a HPC infrastructure with SLURM as scheduler, and conda as tool environment. 

Official documentation for [Snakemake](https://snakemake.readthedocs.io/en/stable/)
Official documentation for [Conda](https://docs.conda.io/en/latest/)

[Documentation for reproducibility](https://snakemake.readthedocs.io/en/stable/snakefiles/deployment.html#distribution-and-reproducibility)


## Requirements

These scripts can run on a HPC infrastructure with SLURM as scheduler using Lmod and conda to manage environments.
Datasets are related to Stategra project analysis by Gomez-Cabrero et al. (2019) 6:256 doi.10.1038/s41597-019-0202-7.

## Connection to a HPC Cluster infrastructure

Connection is done in ssh.


## Project directory in one's home

At the end of the practice, your architecture should now look like this: 

	$ tree
    |-- m2bi_snakemake
        |-- config
           |-- config.yml
        |-- data
            |-- reference -> /home/users/shared/data/Mus_musculus/current/
                |-- bowtie2
                |-- fasta
                     |-- all.fasta
                     |-- chr1.fna
                     |-- chr10.fna
                     |-- ...
                |-- flat
            |-- subset -> /home/users/shared/data/stategra/xxx
                |-- ss_xxx_R1_1.fastq.gz
                |-- ss_xxx_R1_2.fastq.gz
                |-- ss_xxx_R2_1.fastq.gz
                |-- ss_xxx_R2_2.fastq.gz
                |-- ss_...
        |-- logs
        |-- results
            |--FastQC
                |-- ss_...
                |-- ...
        |-- workflows
            |-- envs
                |-- qc.yml
            |-- Snakefile.ex1.smk
            |-- Snakefile.ex2.smk
            |-- ...


## Usage

1. To collect scripts, use the git command line client:

Exemple with HTTPS protocol:

`git clone https://git.mesocentre.uca.fr/nadgoue/m2bi_snakemake.git`

This will create a m2bi_snakemake repository in your `home`.


2. Uploading your environment in HPC-formation cluster

	$ module load gcc/8.1.0 conda/4.12.0 python/3.7.1 snakemake/7.15.1


3. From master node, launching a job is looking like:

	[hpcloging~]$ snakemake --snakefile workflow/Snakefile.smk --jobs 1 --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time}" [--other_options]


## Licence

Creative Commons Legal Code - CC0 1.0 Universal


## Contributing

Contributions are welcome. 
