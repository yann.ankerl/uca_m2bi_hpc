configfile: "config/config.yaml",

rule all:
    input:
        expand("results/FastQC/{sample}_fastqc.html", sample = config["samples"]),
        
rule fastqc:
    input:
         expand("data/subset/{sample}.fastq.gz", sample = config["samples"])
    output:
        expand("results/FastQC/{sample}_fastqc.zip", sample = config["samples"]), 
        expand("results/FastQC/{sample}_fastqc.html", sample = config["samples"])
    conda:
        "envs/qc.yml"
    resources: 
        mem_mb=5000, 
        time="00:30:00"
    shell: "fastqc --outdir results/FastQC/ {input}"
