configfile: "config/config.yaml",

rule all:
    input:
        expand("data/rrbs/{sample}.fastq.gz", sample = config["samples"]),
        expand("data/rrbs/{paired}_1.fastq.gz", paired = config["paired"]),
        expand("data/rrbs/{paired}_2.fastq.gz", paired = config["paired"]),
        expand("results/preTrimming/{sample}.fastqc.zip", sample = config["samples"]),
        expand("results/preTrimming/{sample}.fastqc.html", sample = config["samples"]),
        expand("results/Trimming/{paired}_1_trimmed.fq.gz", paired = config["paired"]),
        expand("results/Trimming/{paired}_2_trimmed.fq.gz", paired = config["paired"]),
        expand("results/Trimming/{paired}_1_val_1.fq.gz", paired = config["paired"]),
        expand("results/Trimming/{paired}_2_val_2.fq.gz", paired = config["paired"]),
        expand("results/postTrimming/{paired}_1_val_1_fastqc.html" , pairede=config["paired"]),
        expand("results/postTrimming/{paired}_1_val_1_fastqc.zip" , paired=config["paired"]),
        expand("results/postTrimming/{paired}_2_val_2_fastqc.html" , paired=config["paired"]),
        expand("results/postTrimming/{paired}_2_val_2_fastqc.zip" , paired=config["paired"])
  




rule fastqc_pre_trim:
    input:
        "data/rrbs/{sample}.fastq.gz"
    output:
        zip = "results/preTrimming/{sample}.fastqc.zip",
        html = "results/preTrimming/{sample}.fastqc.html"
    conda:
        "../config/rrbs_env.yml"
    threads: 2
    resources:
        nodes=4,
        mem_mb=1000,
        time="00:30:00"
    message:
        "Pre-trimming quality control"
    shell:
        """
        mkdir -p results/preTrimming
        fastqc {input} -o "results/preTrimming/" -t {threads}
        """
rule trim:
    input:
        forward = "data/rrbs/{paired}_1.fastq.gz", paired = config["paired"],
        reverse = "data/rrbs/{paired}_2.fastq.gz", paired = config["paired"]
    output:
        trimed1 = "results/Trimming/{paired}_1_trimmed.fq.gz",
        trimed2 = "results/Trimming/{paired}_2_trimmed.fq.gz",
        valid1 = "results/Trimming/{paired}_1_val_1.fq.gz",
        valid2 = "results/Trimming/{paired}_2_val_2.fq.gz"
    conda:
        "../config/rrbs_env.yml"
    threads: 2
    resources:
        nodes=4,
        mem_mb=3000,
        time="00:30:00"
    message:
        "Trimming"
    shell:
        """
        mkdir -p results/Trimming
        trim_galore --rrbs --paired -q 30  --clip_R1 5 --clip_R2 5 \
        --three_prime_clip_R1 3 --three_prime_clip_R2 3 \
	{input.forward} {input.reverse}  -o results/Trimming/
        """

rule fastqc_post_trim:
    input:
        rules.trim.output.valid1,
        rules.trim.output.valid2
    output:
        zip = "results/postTrimming/{paired}_2_val_2.fq.gz.zip",
        html = "results/postTrimming/{paired}_2_val_2.fq.gz.html"
    conda:
        "../config/rrbs_env.yml"
    threads: 2
    resources:
        nodes=4,
        mem_mb=1000,
        time="00:30:00"
    message:
        "Post-trimming quality control"
    shell:
        """
        mkdir -p results/postTrimming
        fastqc {input} -o "results/postTrimming/" -t {threads}
        """
rule bismark_align:
    message: "Performing alignment..."
    input:
        rules.trim.output.valid1,
        rules.trim.output.valid2
    output:
      bam = "results/bismark/{paired}_val_1_bismark_bt2_pe.bam",
      report = "results/bismark/{paired}_val_1_bismark_bt2_PE_report.txt",
      stats = "results/bismark/{paired}__val_1_bismark_bt2_pe.nucleotide_stats.txt"
    conda: 
      "../config/rrbs_env.yml"
    threads: 4
    shell:
      """
      bismark --phred33-quals --bowtie2 -p {threads} \
        --genome "data/current/fasta/all.fasta" --unmapped --ambiguous \
        --ambig_bam --nucleotide_coverage --output_dir "results/bismark/" \
        --fastq --temp_dir {params[0]} -1 {input[0]} -2 {input[1]}
      """
