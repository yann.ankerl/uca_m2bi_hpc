SAMPLES=["ss_50k_0h_R1_1", "ss_50k_0h_R1_2", "ss_50k_0h_R2_1", "ss_50k_0h_R2_2", "ss_50k_24h_R1_1", "ss_50k_24h_R1_2"]

rule all:
    input:
        expand("results/FastQC/{sample}_fastqc.zip", sample=SAMPLES),
        expand("results/FastQC/{sample}_fastqc.html", sample=SAMPLES)
        
rule fastqc:
    input:
        expand("data/subset/{sample}.fastq.gz", sample=SAMPLES),
        expand("data/subset/{sample}.fastq.gz", sample=SAMPLES)
    output:
        expand("results/FastQC/{sample}_fastqc.zip", sample=SAMPLES), 
        expand("results/FastQC/{sample}_fastqc.html", sample=SAMPLES)
    conda:
        "envs/qc.yml"
    resources:
        mem_mb=5000, 
        time="00:30:00"
    shell: "fastqc --outdir results/FastQC/ {input}"
